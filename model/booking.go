package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type Booking struct {
	gorm.Model
	UUID string `json:"uuid"`
	OccupantId uint `json:"occupant_id"`
	CreatedByUserId uint `json:"created_by_user_id"`
	RoomID uint `json:"room_id"`
	PricingID uint `json:"pricing_id"`
	PricingAmount float64 `json:"pricing_amount"`
	BookAt time.Time `json:"book_at"`
	FinishAt time.Time `json:"finish_at"`
	TotalAmount float64 `json:"total_amount"`
	//HOOK
	OutStanding float64 `json:"out_standing"`
	//EAGER
	Room *Room `json:"room,omitempty"`
	Occupant *Occupant `json:"occupant,omitempty" `
	Payment []Payment `json:"payment,omitempty" `
}


func (b Booking)generateUUID() string {
	return "bo"+uuid.New().String()
}

func (b *Booking) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID= b.generateUUID()
	return
}
