package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Building struct {
	gorm.Model
	UUID string `json:"uuid"`
	Name    string `json:"name" validate:"nonzero"`
	Address string `json:"address"  validate:"nonzero"`
	UserID  uint `json:"user_id"  validate:"nonzero"`
	User    *User `json:"user,omitempty" `
	Room []Room ` json:"room,omitempty"`
}

func (b Building)generateUUID() string {
	return "bu"+uuid.New().String()
}

func (b *Building) BeforeCreate(tx *gorm.DB) (err error) {
	b.UUID= b.generateUUID()
	return
}

