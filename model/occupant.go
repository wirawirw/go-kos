package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Occupant struct {
	gorm.Model
	UUID string `json:"uuid"`
	Name string `json:"name"`
	IdCard string `json:"id_card"`
	CardType string `json:"card_type"`
	Email string `json:"email"`
	Phone string `json:"phone"`
	Photo string `json:"photo"`
}
func (o Occupant) generateUUID()string  {
	return "oc"+uuid.New().String()
}

func (o *Occupant) BeforeCreate(tx *gorm.DB) (err error) {
	o.UUID= o.generateUUID()
	return
}
