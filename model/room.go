package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Room struct {
	gorm.Model
	UUID string `json:"uuid"`
	Name        string `json:"name" validate:"nonzero"`
	RoomNumber  int    `json:"room_number" validate:"nonzero"`
	Status      int    `json:"status" validate:"nonzero"`
	Floor       int    `json:"floor" validate:"nonzero"`
	BuildingID  uint   `json:"building_id" validate:"nonzero"`
	Building *Building   `json:"building,omitempty"`
	RoomFeature []RoomFeature `json:"room_feature,omitempty"`
	Booking     []Booking `json:"booking,omitempty"`
	Pricing     []Pricing `json:"pricing,omitempty"`
}

func (r Room) generateUUID() string {
	return "ro"+uuid.New().String()
}

func (r *Room) BeforeCreate(tx *gorm.DB) (err error) {
	r.UUID= r.generateUUID()
	return
}

