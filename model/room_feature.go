package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type RoomFeature struct {
	gorm.Model
	UUID string `json:"uuid"`
	RoomID uint `json:"room_id" validate:"nonzero"`
	FeatureID uint `json:"feature_id" validate:"nonzero"`
	Room *Room `json:"room,omitempty"`
	Feature *Feature `json:"feature,omitempty""`
}

func (rf RoomFeature) generateUUID() string {
	return "rf"+uuid.New().String()
}

func (rf *RoomFeature) BeforeCreate(tx *gorm.DB) (err error) {
	rf.UUID = rf.generateUUID()
	return
}
