package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Pricing struct {
	gorm.Model
	UUID string `json:"uuid"`
	Price float64       `json:"price" validate:"nonzero"`
	DurationType string `json:"duration_type" validate:"nonzero"` //month,day,year
	RoomID uint `json:"room_id" validate:"nonzero" validate:"nonzero"`
	Room *Room `json:"room,omitempty"`
}
func (p Pricing) generateUUID() string {
	return "pr"+uuid.New().String()
}

func (p *Pricing) BeforeCreate(tx *gorm.DB) (err error) {
	p.UUID= p.generateUUID()
	return
}

