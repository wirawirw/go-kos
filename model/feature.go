package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Feature struct {
	gorm.Model
	UUID string `json:"uuid"`
	Name string `validate:"nonzero",json:"name",gorm:"not null"`
}

func (f Feature) generateUUID() string {
	return "fe"+uuid.New().String()
}

func (f *Feature) BeforeCreate(tx *gorm.DB) (err error) {
	f.UUID= f.generateUUID()
	return
}
