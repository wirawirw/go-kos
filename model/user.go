package model

import (
	"github.com/google/uuid"
	"go-kos/services"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	UUID string `json:"uuid"`
	Name string `json:"name" gorm:"not null"`
	Email string `json:"email" gorm:"unique,not null"`
	Password string `json:"password"`
}

func (u User) generateUUID() string {
	return "us"+uuid.New().String()
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.UUID=u.generateUUID()
	u.Password= string(services.Bcrypting(u.Password))
	return
}

