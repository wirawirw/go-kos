package model

import "gorm.io/gorm"

type Payment struct {
	gorm.Model
	TrxID         string       `json:"trx_id"`
	PaymentTypeID uint         `json:"payment_type_id"`
	PaymentType   *PaymentType `json:"payment_type,omitempty" "`
	BookingID     uint         `json:"booking_id"`
	Booking       *Booking     `json:"booking,omitempty" `
}




