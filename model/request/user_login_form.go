package request

type LoginForm struct {
	Email string `form:"email" validate:"nonzero"`
	Password string `form:"password" validate:"nonzero"`
}
