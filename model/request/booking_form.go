package request

type BookingForm struct {
	OccupantId uint `json:"occupant_id" validate:"nonzero"`
	RoomID uint `json:"room_id" validate:"nonzero"`
	PricingID uint `json:"pricing_id" validate:"nonzero"`
	BookAt string `json:"book_at" validate:"nonzero"`
}

