package building_controller

import (
	"github.com/gin-gonic/gin"
	"go-kos/model/response"
	"go-kos/usecase/building"
	"gopkg.in/validator.v2"
)

func Index(ctx *gin.Context)  {
	var b building.UBuilding

	err,r:=b.Fetch()
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}
	response.OkWithData(r,ctx)
}

func Show(ctx *gin.Context)  {
	var b building.UBuilding

	err,r:=b.Show(ctx.Param("id"))
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}
	response.OkWithData(r,ctx)
}

func Store(ctx *gin.Context)  {
	var b building.UBuilding

	ctx.ShouldBindJSON(&b.BuildingModel)

	err:=validator.Validate(b.BuildingModel)
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		if err:=b.Store();err != nil {
			response.FailWithMessage(err.Error(),ctx)
		}else{
			response.OkWithData(b,ctx)
		}
	}
}

func Update(ctx *gin.Context)  {
	var b building.UBuilding

	ctx.ShouldBindJSON(&b.BuildingModel)
	err:=validator.Validate(b.BuildingModel)
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		if err:=b.Update();err != nil {
			response.FailWithDetailed(err,err.Error(),ctx)
		}else{
			response.OkWithData(b.BuildingModel,ctx)
		}
	}
}