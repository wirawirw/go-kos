package auth_controller

import (
	"github.com/gin-gonic/gin"
	"go-kos/model/request"
	"go-kos/model/response"
	"go-kos/services"
	"go-kos/usecase/auth"
	"gopkg.in/validator.v2"
)

func Login(ctx *gin.Context)  {
	loginForm:= request.LoginForm{}
	ctx.ShouldBindJSON(&loginForm)

	if err:=validator.Validate(loginForm);err != nil {
		response.FailWithDetailed(err,err.Error(),ctx)
	}

	user:=auth.UAuthUser{
		FormLogin:        loginForm,
	}

	u,e:=user.Login()

	if e != nil {
		response.FailWithMessage(e.Error(),ctx)
	}else {
		if token,err:=services.GenerateJwtToken(u.ID);err != nil {
			response.FailWithMessage(err.Error(),ctx)
		}else{
			response.OkWithData(token,ctx)
		}
	}
}
