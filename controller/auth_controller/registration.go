package auth_controller

import (
	"github.com/gin-gonic/gin"
	"go-kos/model/request"
	"go-kos/model/response"
	"go-kos/usecase/auth"
	"gopkg.in/validator.v2"
)

func Registration(ctx *gin.Context)  {
	var userForm request.UserRegistrationForm

	if err:=ctx.ShouldBindJSON(&userForm); err!= nil {
		response.FailWithMessage(err.Error(),ctx)
	}

	if err:=validator.Validate(userForm);err != nil {
		response.FailWithDetailed(err,err.Error(),ctx)
	}

	authRegisterUser:=auth.UAuthUser{
		FormRegistration: userForm,
	}

	if err:=authRegisterUser.Register();err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}
}
