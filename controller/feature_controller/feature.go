package feature_controller

import (
	"github.com/gin-gonic/gin"
	"go-kos/model/response"
	"go-kos/usecase/feature"
	"gopkg.in/validator.v2"
)


func Index(ctx *gin.Context)  {
	var uf feature.UFeature


	if err,features:=uf.Fetch() ;err!= nil {
		response.FailWithDetailed(err,err.Error(),ctx)
	}else{
		response.OkWithData(features,ctx)
	}
}

func Show(ctx *gin.Context)  {
	var uf feature.UFeature

	if err,feature:=uf.Show(ctx.Param("id")) ;err!= nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		response.OkWithData(feature,ctx)
	}
}

func Update(ctx *gin.Context)  {
	var uf feature.UFeature

	ctx.ShouldBindJSON(&uf.Feature)
	err:=validator.Validate(uf.Feature)
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		if err:=uf.Update();err != nil {
			response.FailWithDetailed(err,err.Error(),ctx)
		}else{
			response.OkWithData(uf.Feature,ctx)
		}
	}

}

func Store(ctx *gin.Context)  {
	var uf feature.UFeature

	ctx.ShouldBindJSON(&uf.Feature)
	if err:=validator.Validate(uf.Feature);err != nil {
		response.FailWithDetailed(err,err.Error(),ctx)
	}else{
		if err:=uf.Store(); err!= nil {
			response.FailWithDetailed(err,err.Error(),ctx)
		}else{
			response.OkWithData(uf.Feature,ctx)
		}
	}
}