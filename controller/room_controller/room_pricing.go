package room_controller

import (
	"github.com/gin-gonic/gin"
	"go-kos/model/response"
	"go-kos/usecase/room"
	"gopkg.in/validator.v2"
)

func StoreRoomPricing(ctx *gin.Context)  {
	ur:=room.URoom{}
	if err:=ctx.ShouldBindJSON(&ur.Pricing);err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		if err:=validator.Validate(ur.Pricing);err != nil {
			response.FailWithMessage(err.Error(),ctx)
		}else{
			if err:=ur.StorePricingRoom();err!= nil {
				response.FailWithMessage(err.Error(),ctx)
			}else{
				response.OkWithData(ur.Pricing,ctx)
			}
		}
	}
}