package room_controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go-kos/model/response"
	"go-kos/usecase/room"
	"gopkg.in/validator.v2"
)


func Index(ctx *gin.Context)  {
	fmt.Println(ctx.GetString("auth_user_id"))
	var ur room.URoom

	err,r:=ur.Fetch()
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		response.OkWithData(r,ctx)
	}
}

func Store(ctx *gin.Context)  {
	var ur room.URoom

	if err:=ctx.ShouldBindJSON(&ur.Room);err != nil {
		response.FailWithDetailed(err,err.Error(),ctx)
	}

	if err:=validator.Validate(ur.Room);err!=nil {
		response.FailWithDetailed(err,err.Error(),ctx)
	}else{
		if err:=ur.Store();err != nil {
			response.FailWithDetailed(err,err.Error(),ctx)
		}else{
			response.OkWithData("Data stored",ctx)
		}
	}
}

func Show(ctx *gin.Context)  {
	var ur room.URoom

	err,r:=ur.Show(ctx.Param("id"))
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		response.OkWithData(r,ctx)
	}
}

func Update(ctx *gin.Context)  {
	var ur room.URoom

	err:=ctx.ShouldBindJSON(&ur.Room)
	if err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		if err:=ur.Update() ;err!= nil {
			response.FailWithDetailed(err,err.Error(),ctx)
		}else{
			response.OkWithData("Data updated",ctx)
		}
	}
}