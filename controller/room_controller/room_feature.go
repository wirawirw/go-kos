package room_controller

import (
	"github.com/gin-gonic/gin"
	"go-kos/model/response"
	"go-kos/usecase/room"
	"gopkg.in/validator.v2"
)

func AddRoomFeature(ctx *gin.Context)  {
	ur:=room.URoom{}
	if err:=ctx.ShouldBindJSON(&ur.RoomFeature);err != nil {
		response.FailWithMessage(err.Error(),ctx)
	}else{
		if err:=validator.Validate(ur.RoomFeature);err != nil {
			response.FailWithMessage(err.Error(),ctx)
		}else{
			if err:=ur.AddFeatureRoom();err!= nil {
				response.FailWithMessage(err.Error(),ctx)
			}else{
				response.OkWithData("Room Feature Updated",ctx)
			}
		}
	}
}
