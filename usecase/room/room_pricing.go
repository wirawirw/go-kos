package room

import "go-kos/infrastructure/database"

func (ur *URoom) StorePricingRoom() error {
	return database.DB.Create(&ur.Pricing).Error
}
