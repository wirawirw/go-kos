package room

import (
	"go-kos/model"
	"reflect"
	"testing"
)

func TestURoom_Fetch(t *testing.T) {
	type fields struct {
		Room model.Room
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr error
		wantR   []model.Room
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ur := URoom{
				Room: tt.fields.Room,
			}
			gotErr, gotR := ur.Fetch()
			if !reflect.DeepEqual(gotErr, tt.wantErr) {
				t.Errorf("Fetch() gotErr = %v, want %v", gotErr, tt.wantErr)
			}
			if !reflect.DeepEqual(gotR, tt.wantR) {
				t.Errorf("Fetch() gotR = %v, want %v", gotR, tt.wantR)
			}
		})
	}
}

func TestURoom_Show(t *testing.T) {
	type fields struct {
		Room model.Room
	}
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr error
		wantR   model.Room
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ur := URoom{
				Room: tt.fields.Room,
			}
			gotErr, gotR := ur.Show(tt.args.id)
			if !reflect.DeepEqual(gotErr, tt.wantErr) {
				t.Errorf("Show() gotErr = %v, want %v", gotErr, tt.wantErr)
			}
			if !reflect.DeepEqual(gotR, tt.wantR) {
				t.Errorf("Show() gotR = %v, want %v", gotR, tt.wantR)
			}
		})
	}
}

func TestURoom_Store(t *testing.T) {
	type fields struct {
		Room model.Room
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ur := URoom{
				Room: tt.fields.Room,
			}
			if err := ur.Store(); (err != nil) != tt.wantErr {
				t.Errorf("Store() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestURoom_Update(t *testing.T) {
	type fields struct {
		Room model.Room
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ur := &URoom{
				Room: tt.fields.Room,
			}
			if err := ur.Update(); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
