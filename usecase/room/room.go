package room

import (
	"go-kos/infrastructure/database"
	"go-kos/model"
)

type URoom struct {
	Room model.Room
	RoomFeature model.RoomFeature
	Pricing model.Pricing
}

//get all room
func (ur URoom) Fetch()(error, []model.InterfaceModel)  {
	rooms:=[]model.Room{}
	err:=database.DB.Preload("RoomFeature.Feature").Preload("Pricing").Find(&rooms).Error

	var interfaceSlice = make([]model.InterfaceModel, len(rooms))
	for i, d := range rooms {
		interfaceSlice[i] = d
	}

	return err,interfaceSlice
}

//show detail one room
func (ur URoom) Show(id string)(error,model.InterfaceModel)  {
	room:=model.Room{}
	err:=database.DB.Preload("RoomFeature.Feature").Preload("Pricing").First(&room,id).Error
	return err,room
}

//store room
func (ur URoom) Store()(err error)  {
	err=database.DB.Create(&ur.Room).Error
	return
}

//update data and return result to receiver pointer
func (ur *URoom) Update()(err error)  {
	return database.DB.Model(&ur.Room).Updates(ur.Room).First(&ur.Room).Error
}


