package usecase

import "go-kos/model"

type UsecaseInterface interface {
	Fetch()(error,[]model.InterfaceModel)
	Show(id string) (error,model.InterfaceModel)
	Store() (error)
	Update() (error)
}

