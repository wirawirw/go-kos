package booking

import (
	"go-kos/infrastructure/database"
	"go-kos/model"
	"go-kos/model/request"
)

type UBooking struct {
	Booking model.Booking
	request.BookingForm
}

func (ub UBooking) Fetch()(error,[]model.InterfaceModel)  {
	bookings:=[]model.Booking{}
	err:=database.DB.Find(&bookings).Error

	var interfaceSlice = make([]model.InterfaceModel, len(bookings))
	for i, d := range bookings {
		interfaceSlice[i] = d
	}
	return err,interfaceSlice
}

func (ub UBooking) Show(id string) (error,model.InterfaceModel) {
	booking:=model.Booking{}
	err:=database.DB.First(&booking).Error
	return err,booking

}

func (ub UBooking) Store() (error)  {
	err:=database.DB.Create(ub.Booking).Error
	return err
}

func (ub UBooking) Update() (error)  {
	err:=database.DB.Updates(ub.Booking).Error
	return err
}
