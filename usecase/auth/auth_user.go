package auth

import (
	"go-kos/infrastructure/database"
	"go-kos/model"
	"go-kos/model/request"
	"go-kos/services"
)

type UAuthUser struct {
	FormRegistration request.UserRegistrationForm
	FormLogin        request.LoginForm
}

//Convert dto(data to object) to model and create record
func (u UAuthUser) Register() error {
	entityUser:=model.User{
		Name:     u.FormRegistration.Name,
		Email:    u.FormRegistration.Email,
		Password: u.FormRegistration.Password,
	}
	return database.DB.Create(&entityUser).Error
}

//check email and compare password
func (u UAuthUser) Login()(user model.User,err error)  {
	err=database.DB.Where("email = ?",u.FormLogin.Email).First(&user).Error
	err=services.BcryptCheck(u.FormLogin.Password,user.Password)
	return
}
