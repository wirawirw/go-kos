package auth

import (
	"go-kos/model"
	"go-kos/model/request"
	"reflect"
	"testing"
)

func TestUAuthUser_Login(t *testing.T) {
	type fields struct {
		FormRegistration request.UserRegistrationForm
		FormLogin        request.LoginForm
	}
	tests := []struct {
		name     string
		fields   fields
		wantUser model.User
		wantErr  bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UAuthUser{
				FormRegistration: tt.fields.FormRegistration,
				FormLogin:        tt.fields.FormLogin,
			}
			gotUser, err := u.Login()
			if (err != nil) != tt.wantErr {
				t.Errorf("Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotUser, tt.wantUser) {
				t.Errorf("Login() gotUser = %v, want %v", gotUser, tt.wantUser)
			}
		})
	}
}

func TestUAuthUser_Register(t *testing.T) {
	type fields struct {
		FormRegistration request.UserRegistrationForm
		FormLogin        request.LoginForm
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UAuthUser{
				FormRegistration: tt.fields.FormRegistration,
				FormLogin:        tt.fields.FormLogin,
			}
			if err := u.Register(); (err != nil) != tt.wantErr {
				t.Errorf("Register() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
