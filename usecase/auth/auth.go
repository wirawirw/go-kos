package auth

import "go-kos/model"

type UsecaseAuth interface {
	Register() error
	Login()(user model.User,err error)
}
