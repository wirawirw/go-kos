package building

import (
	"go-kos/infrastructure/database"
	"go-kos/model"
)


type UBuilding struct {
	BuildingModel model.Building
}

//get all building
func (ub UBuilding) Fetch() ( error,[]model.InterfaceModel)  {
	b:=[]model.Building{}
	err:=database.DB.Joins("User").Find(&b).Error

	var interfaceSlice = make([]model.InterfaceModel, len(b))
	for i, d := range b {
		interfaceSlice[i] = d
	}

	return err,interfaceSlice
}

//get one building
func (ub UBuilding) Show(id string) (error,model.InterfaceModel) {
	building:=model.Building{}
	err:=database.DB.Joins("User").First(&building,id).Error
	return err,building
}

//store building
func (ub UBuilding) Store() (error) {
	err:= database.DB.Create(&ub.BuildingModel).Error
	return err
}

//update exist building, and set pointer ubuilding to record result
func (ub *UBuilding) Update() (error) {
	err:=database.DB.Model(&ub.BuildingModel).Updates(&ub.BuildingModel).Error
	return err
}



