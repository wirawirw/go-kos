package building

import (
	"go-kos/model"
	"reflect"
	"testing"
)

func TestUBuilding_Fetch(t *testing.T) {
	type fields struct {
		BuildingModel model.Building
	}
	tests := []struct {
		name   string
		fields fields
		want   error
		want1  []model.InterfaceModel
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ub := UBuilding{
				BuildingModel: tt.fields.BuildingModel,
			}
			got, got1 := ub.Fetch()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Fetch() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Fetch() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestUBuilding_Show(t *testing.T) {
	type fields struct {
		BuildingModel model.Building
	}
	type args struct {
		id string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   error
		want1  model.InterfaceModel
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ub := UBuilding{
				BuildingModel: tt.fields.BuildingModel,
			}
			got, got1 := ub.Show(tt.args.id)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Show() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Show() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestUBuilding_Store(t *testing.T) {
	type fields struct {
		BuildingModel model.Building
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ub := UBuilding{
				BuildingModel: tt.fields.BuildingModel,
			}
			if err := ub.Store(); (err != nil) != tt.wantErr {
				t.Errorf("Store() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUBuilding_Update(t *testing.T) {
	type fields struct {
		BuildingModel model.Building
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ub := UBuilding{
				BuildingModel: tt.fields.BuildingModel,
			}
			if err := ub.Update(); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
