package feature

import (
	"go-kos/infrastructure/database"
	"go-kos/model"
)

type UFeature struct {
	model.Feature
}

func (uf UFeature)Fetch()( error,[]model.InterfaceModel){
	var feature []model.Feature
	err:=database.DB.Find(&feature).Error
	var interfaceSlice = make([]model.InterfaceModel, len(feature))
	for i, d := range feature {
		interfaceSlice[i] = d
	}
	return err,interfaceSlice
}

func (uf UFeature) Show(id string)(error,model.InterfaceModel)  {
	f:=model.Feature{}
	err:=database.DB.First(&f,id).Error
	return err,f
}

func (uf *UFeature) Store() (err error) {
	err=database.DB.Create(&uf.Feature).Error
	return
}

func (uf *UFeature) Update() (err error)  {
	err=database.DB.Updates(&uf.Feature).Error
	return
}