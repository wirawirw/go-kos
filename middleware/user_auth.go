package middleware

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"go-kos/model/response"
	"go-kos/services"
)

func AuthUserMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		tokenString := ctx.Request.Header.Get("Authorization")

		err,token:=services.ParseJwtToken(tokenString)
		if  err!= nil {
			response.FailWithDetailed(err,err.Error(),ctx)
			ctx.Abort()
		}else{
			userInfo:=token.Claims.(jwt.MapClaims)
			fmt.Println(userInfo["user_id"])

			ctx.Set("auth_user_id",userInfo["user_id"])
		}
	}
}
