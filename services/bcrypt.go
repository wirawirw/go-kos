package services

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

// Hashing the password with the default cost of 10
func Bcrypting(password string) ([]byte) {
	bytePassword:=[]byte(password)
	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	return hashedPassword
}

//Comparing the password with the hash,nil means it is a match
func BcryptCheck(hashedPassword string,password string) (err error) {
	fmt.Println([]byte(password))
	err = bcrypt.CompareHashAndPassword( []byte(password),[]byte(hashedPassword))
	return
}


