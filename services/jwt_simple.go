package services

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

func GenerateJwtToken(userId uint) (token string,err error)  {
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userId
	atClaims["exp"] = time.Now().Add(time.Minute * 360).Unix()

	sign := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"),atClaims)
	token, err = sign.SignedString([]byte("secret"))
	return
}

func ParseJwtToken(tokenString string) (err error,token *jwt.Token,) {
	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("secret"), nil
	})

	return
}