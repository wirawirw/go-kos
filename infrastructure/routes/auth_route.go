package routes

import (
	"github.com/gin-gonic/gin"
	"go-kos/controller/auth_controller"
)

func AuthRoute(rg *gin.RouterGroup)  {
	a:=rg.Group("auth")
	a.POST("login", auth_controller.Login)
	a.POST("register", auth_controller.Registration)
}
