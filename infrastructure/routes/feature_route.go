package routes

import (
	"github.com/gin-gonic/gin"
	"go-kos/controller/feature_controller"
)

func FeatureRoute(rg *gin.RouterGroup)  {
	rf:=rg.Group("feature")
	rf.GET("",feature_controller.Index)
	rf.GET(":id",feature_controller.Show)
	rf.POST("store",feature_controller.Store)
	rf.POST("update",feature_controller.Update)
}