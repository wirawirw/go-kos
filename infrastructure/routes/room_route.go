package routes

import (
	"github.com/gin-gonic/gin"
	"go-kos/controller/room_controller"
)

func RoomRoute(rg *gin.RouterGroup)  {
	rr:=rg.Group("room")
	rr.GET("",room_controller.Index)
	rr.GET(":id",room_controller.Show)
	rr.POST("store",room_controller.Store)
	rr.POST("update",room_controller.Update)
	rr.POST("feature/add",room_controller.AddRoomFeature)
	rr.POST("pricing/store",room_controller.StoreRoomPricing)
}