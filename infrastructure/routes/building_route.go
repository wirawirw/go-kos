package routes

import (
	"github.com/gin-gonic/gin"
	"go-kos/controller/building_controller"
)

func BuildingRoute(rg *gin.RouterGroup)  {
	b:=rg.Group("building")
	b.GET("", building_controller.Index)
	b.GET(":id",building_controller.Show)
	b.POST("store",building_controller.Store)
	b.POST("update",building_controller.Update)
}
