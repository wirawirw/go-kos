package migrations

import (
	database "go-kos/infrastructure/database"
	"go-kos/model"
)

func Migrate()  {
	database.DB.AutoMigrate(&model.Pricing{})
	database.DB.AutoMigrate(&model.Building{})
	database.DB.AutoMigrate(&model.User{})
	database.DB.AutoMigrate(&model.RoomFeature{})
	database.DB.AutoMigrate(&model.Room{})
	database.DB.AutoMigrate(&model.Booking{})
	database.DB.AutoMigrate(&model.Room{})
	database.DB.AutoMigrate(&model.Feature{})
	database.DB.AutoMigrate(&model.Payment{})
	database.DB.AutoMigrate(&model.Occupant{})
}