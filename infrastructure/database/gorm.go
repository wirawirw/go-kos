package database

import (
	"fmt"
	"go-kos/infrastructure/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func SetupDB() {
	dsn := config.DBAccount+":"+config.DBPass+"@tcp("+config.DBIPEnv+":"+config.DBPortEnv+")/"+config.DBEnv+config.CharsetEnv
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err!=nil {
		fmt.Println("Failed to connect DB")
	}

	DB =db


}
