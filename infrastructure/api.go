package infrastructure

import (
	"github.com/gin-gonic/gin"
	"go-kos/infrastructure/routes"
	"go-kos/middleware"
)
func Dispatch()  {
	rg:=gin.Default()

	rg.Use(middleware.CORSMiddleware())

	v1:=rg.Group("/api/v1")
	routes.AuthRoute(v1)

	v1.Use(middleware.AuthUserMiddleware())
	routes.BuildingRoute(v1)
	routes.RoomRoute(v1)
	routes.FeatureRoute(v1)

	rg.Run(":9990")
}
