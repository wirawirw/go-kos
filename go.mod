module go-kos

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis/v8 v8.11.3
	github.com/google/uuid v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/validator.v2 v2.0.0-20210331031555-b37d688a7fb0
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.14
)
