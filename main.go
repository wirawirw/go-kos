package main

import (
	"go-kos/infrastructure"
	database "go-kos/infrastructure/database"
	migrations "go-kos/infrastructure/database/migrations"
	"go-kos/infrastructure/redis"
)

func main()  {
	database.SetupDB()
	migrations.Migrate()
	redis.InitRedis()
	infrastructure.Dispatch()
}